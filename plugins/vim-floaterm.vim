let g:floaterm_keymap_toggle = '<F12>'
let g:floaterm_title=''

" Floaterm
let g:floaterm_autoinsert=1
let g:floaterm_width=0.9
let g:floaterm_height=0.9
let g:floaterm_wintitle=0
let g:floaterm_autoclose=1