let g:startify_session_dir = '~/.config/nvim/session'

let g:startify_lists = [
          \ { 'type': 'bookmarks', 'header': ['   Bookmarks']                    },
          \ ]

" Not required anymore
"         \ { 'type': 'files',     'header': ['   Files']                        },
          \ { 'type': 'dir',       'header': ['   Current Directory '. getcwd()] },
          \ { 'type': 'sessions',  'header': ['   Sessions']                     },


let g:startify_session_autoload = 1
let g:startify_session_delete_buffers = 1
let g:startify_change_to_vcs_root = 1
let g:startify_fortune_use_unicode = 1
let g:startify_session_persistence = 1

let g:webdevicons_enable_startify = 1

let g:startify_bookmarks = [
            \ { 'pa': '/home/hiren/mydata/docker-data/www/api-admin-dev.purooz.com' },
            \ { 'pad': '/home/hiren/mydata/docker-data/www/admin-dev.purooz.com' },
            \ { 'p': '/home/hiren/mydata/docker-data/www/dev.purooz.com' },
            \ { 'ya': '/home/hiren/mydata/docker-data/www/api-admin-dev.yellowmeducation.com' },
            \ { 'yad': '/home/hiren/mydata/docker-data/www/admin-dev.yellowmeducation.com' },
            \ { 'ma': '/home/hiren/mydata/docker-data/www/api-dev-clearlyinteriors.managemepro.com' },
            \ { 'm': '/home/hiren/mydata/docker-data/www/dev-clearlyinteriors.managemepro.com' },
            \ { 'c': '/home/hiren/mydata/docker-data/www/dev.carepreference.com' },
            \ ]

let g:startify_enable_special = 0